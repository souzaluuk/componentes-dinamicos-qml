import QtQuick 2.12
import QtQuick.Controls 2.12

ApplicationWindow {
    width: 360
    height: 640
    visible: true
    title: qsTr("Lista Dinâmica")
    Column {
        anchors.fill: parent
        anchors.margins: 10
        spacing: 10
        Label {
            width: parent.width
            elide: Label.ElideRight
            text: "Selecione o tipo de de planta:*"
        }
        ComboBox {
            id: comboBox
            width: parent.width
            model: ["Árvore", "Palmeira"]
            onCurrentTextChanged: {
                const sources = {
                    "Árvore": componentArvores,
                    "Palmeira": componentPalmeiras
                }
                loader.sourceComponent = sources[currentText]
            }
        }
        Loader {
            id: loader
            width: parent.width
        }
        Component {
            id: componentArvores
            Column {
                spacing: 10
                Label {
                    width: parent.width
                    elide: Label.ElideRight
                    text: "Selecione:*"
                }
                ComboBox {
                    width: parent.width
                    model: ["Andiroba", "Breu Branco", "Cacau", "Capoteiro"]
                }
                Label {
                    width: parent.width
                    elide: Label.ElideRight
                    text: "Quantidade de árvores médias:*"
                }
                TextField {
                    width: parent.width
                }
                Label {
                    width: parent.width
                    elide: Label.ElideRight
                    text: "Quantidade de árvores finas:*"
                }
                TextField {
                    width: parent.width
                }
                Label {
                    width: parent.width
                    elide: Label.ElideRight
                    text: "Quantidade de árvores grossas:*"
                }
                TextField {
                    width: parent.width
                }
            }
        }
        Component {
            id: componentPalmeiras
            Column {
                spacing: 10
                Label {
                    width: parent.width
                    elide: Label.ElideRight
                    text: "Selecione:*"
                }
                ComboBox {
                    width: parent.width
                    model: ["Buriti", "Murumuru", "Paxiúba"]
                }
                Label {
                    width: parent.width
                    elide: Label.ElideRight
                    text: "Quantidade de palmeiras jovens:*"
                }
                TextField {
                    width: parent.width
                }
                Label {
                    width: parent.width
                    elide: Label.ElideRight
                    text: "Quantidade de palmeiras adultas:*"
                }
                TextField {
                    width: parent.width
                }
            }
        }
    }
}
